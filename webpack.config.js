const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

module.exports = {
	entry: './src/app.js',

	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	},

	module: {
		loaders: [
			{
				test: /\.js$/,
				loader: 'buble-loader',
				include: path.resolve(__dirname, 'src'),
				query: {
					objectAssign: 'Object.assign'
				}
			}
		],

		rules: [{
			test: /\.scss$/,
			use: ExtractTextPlugin.extract({ fallback: 'style-loader' , use: ['css-loader', 'sass-loader'] })
		}]
	},

	watch: true,

	devServer: {
		hot: true,
		inline: true,
		contentBase: path.resolve(__dirname, 'dist'),
		watchContentBase: true,
		compress: true,
		publicPath: '/'
	},

	devtool: "source-map",

	plugins: [

			new HtmlWebpackPlugin({
				title: "My Grid",
				template: path.resolve(__dirname, 'src/index.html'),
				// inject: 'body',
				// hash: true
			}),

			new ExtractTextPlugin({
				filename: 'app.css'
			})
	]

}
