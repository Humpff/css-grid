const Sound = require('./Sound.js');
const context = new (window.AudioContext || window.webkitAudioContext)();
const note = new Sound(context);

let now = context.currentTime;

// These are the notes in Hertz


var playButton = document.querySelector('.play');
playButton.addEventListener('click', playSound);

function playSound() {
	note.play(261.63, now);
	note.play(293.66, now + 0.5);
	note.play(329.63, now + 1);
	note.play(349.23, now + 1.5);
	note.play(392.00, now + 2);
	note.play(440.00, now + 2.5);
	note.play(493.88, now + 3);
	note.play(523.25, now + 3.5);
}
